let style = document.getElementById('mainTheme');

document.addEventListener('DOMContentLoaded', () => {

if (localStorage.mainTheme === "alternative") {
    style.href = "css/styleAlt.css";
}

})

let altTheme = document.querySelector('.color-change-btn');

altTheme.addEventListener('click', () => {
    if (localStorage.mainTheme === "alternative") {
        localStorage.mainTheme = "";
        style.href = "css/style.css";
    } else {
        localStorage.mainTheme = "alternative";
        style.href = "css/styleAlt.css";
    }
})
